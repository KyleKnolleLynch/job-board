/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
            {
                hostname: '3xzxzkzqrsy5skot.public.blob.vercel-storage.com'
            }
        ]
    }
};

export default nextConfig;
