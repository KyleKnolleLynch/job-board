export default function Footer() {
  return (
    <footer className='border-t'>
      <div className='mx-auto max-w-5xl px-3 py-5 text-center'>
        <p>@ {new Date().getFullYear()} Job Board, Inc. All rights reserved.</p>
      </div>
    </footer>
  )
}
