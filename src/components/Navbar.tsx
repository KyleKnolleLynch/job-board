import Image from 'next/image'
import Link from 'next/link'

import logo from '@/assets/logo.svg'
import { Button } from './ui/button'

export default function Navbar() {
  return (
    <header className='shadow-sm'>
      <nav className='m-auto flex max-w-5xl items-center justify-between px-3 py-5'>
        <Link href='/' className='flex items-center gap-3'>
          <Image src={logo} alt='Job board logo' width={40} height={40} className='aspect-square' />
          <span className='text-xl font-bold tracking-tight'>Job Board</span>
        </Link>
        <Button asChild>
          <Link href='/jobs/new'>Post a job</Link>
        </Button>
      </nav>
    </header>
  )
}
