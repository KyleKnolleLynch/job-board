# Job Board App

🔗 [Live project](https://job-board-8baj.vercel.app/)

Job search board where you can filter through and search for jobs. Access the hidden admin panel by going to https://job-board-8baj.vercel.app/admin and sign in with Clerk authentication to add a new job, then accept or delete it before it posts. 

This project uses: 

* Next.js 

* app router  

* server actions

* prisma 

* vercel storage

* react hook form

* zod

* Tailwindcss

* Shadcn-ui

### Big thanks and credit to [Florian @ Coding in Flow](https://www.youtube.com/@codinginflow/videos) for this code base and tutorial. 

I jumped ahead and tried to re-create as much of this project as I could on my own, but referred back to the tutorial when I got stuck or didn't understand something fully. 